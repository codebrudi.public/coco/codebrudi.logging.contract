﻿namespace Codebrudi.Logging.Contract;

public interface ILogger<T>
{
    void LogTrace(string message);
    void LogInformation(string message);
    void LogWarning(string message);
    void LogWarning(string message, Exception exception);
    void LogError(string message);
    void LogError(string message, Exception exception);
}
